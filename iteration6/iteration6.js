// Crea una función que reciba por parámetro un array y compruebe si existen elementos duplicados, 
// en caso que existan los elimina para retornar un array sin los elementos duplicados. 
// Puedes usar este array para probar tu función:

const duplicates = [
    "sushi", //no
    "pizza", //si
    "burger", //no
    "potatoe", //no
    "pasta", //si
    "ice-cream", //no
    "pizza", //si
    "chicken", //no
    "onion rings", //no
    "pasta", //si
    "soda", //no
  ];
  
  function removeDuplicates(elemets) {
    var noRemove = [...new Set(elemets)];
    console.log(noRemove);
  }
  
  removeDuplicates(duplicates);
  