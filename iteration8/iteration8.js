// Crea una función que nos devuelva el número de veces que se repite cada una de las palabras que lo conforma.
// Puedes usar este array para probar tu función:

const counterWords = ["code", "repeat", "eat", "sleep", "code", "enjoy", "sleep", "code", "enjoy", "upgrade", "code"];


function repeatCounter2(count) {
    var repetidos = {};
    count.forEach(function(e){
      repetidos[e] = (repetidos[e] || 0) + 1;
    });
    console.log(repetidos);
  }
  
  
repeatCounter2(counterWords)
