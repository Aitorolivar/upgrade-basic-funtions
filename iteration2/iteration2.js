// Completa la función que tomando un array de strings como argumento devuelva el más largo,
// en caso de que dos strings tenga la misma longitud deberá devolver el primero.

// Puedes usar este array para probar tu función:

const avengers = ["Hulk", "Thor", "IronMan", "Captain A.", "Spiderman", "Captain M."];


function findLongestWord(heroes) {
let total = heroes[0]
    for(let hero of heroes){
        if(hero.length > total.length){
           total = hero;
        };
    };
  console.log(total)
}

findLongestWord(avengers);
